/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  ListView
} from "react-native";
import ComponenteTexto from "./ComponenteTexto";
import ChildComponent from "./ChildComponent";

const instructions = Platform.select({
  ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
  android:
    "Double tap R on your keyboard to reload,\n" +
    "Shake or press menu button for dev menu, \n"
});

type Props = {};
export default class App extends Component<Props> {
  constructor() {
    super();

    this.state = {
      status: false,
      data: null
    };
  }
  componentDidMount = () => {
    fetch("https://facebook.github.io/react-native/movies.json")
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          data: responseJson.movies
        });
      });
  };
  clicked = () => {
    this.setState({ status: !this.state.status });
  };

  render() {
    return (
      <View style={styles.container}>
        <ComponenteTexto />
        <ChildComponent status={this.state.status} result={this.state.data} />
        <Button
          onPress={() => {
            this.clicked(this);
          }}
          title="Click Here"
          color="red"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ccc"
  },
  welcome: {
    fontSize: 24,
    textAlign: "center",
    color: "black",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "purple",
    marginBottom: 5
  }
});
