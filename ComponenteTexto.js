import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";

export default class ComponenteTexto extends Component {
  render() {
    return (
      <View>
        <Text style={styles.text}>Esto es un Componente de Texto Externo</Text>
        <Text>{10 + 5}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    color: "red"
  }
});
