import React, { Component } from "react";
import { StyleSheet, Text } from "react-native";

export default class Loading extends Component {
  render() {
    return <Text style={styles.text}>Cargando...</Text>;
  }
}

const styles = StyleSheet.create({
  text: {
    color: "#0000"
  }
});
