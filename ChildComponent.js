import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import Loading from "./Loading";

export default class ChildComponent extends Component {
  constructor() {
    super();
  }
  render() {
    if (this.props.result) {
      var res = this.props.result.map((item, i) => {
        return <Text key={i}>{item.title}</Text>;
      });
    }
    return (
      <View style={styles.container}>
        {this.props.result ? res : <Loading />}
        <View style={this.props.status ? styles.on : styles.off} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 3
  },
  on: {
    backgroundColor: "green",
    width: 100,
    height: 100
  },
  off: {
    backgroundColor: "grey",
    width: 100,
    height: 100
  }
});
